#include <stdint.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <cstdlib>
#include <fstream>

#include "NSLModule.hh"

using namespace std;

void show_usage( void )
{
  cerr	<< "Usage: " << endl
	<< "./LUT <board address> c <filename>					: configure LUT" << endl
	<< "./LUT <board address> d <filename> <sectorID> <LUT_type> <RoI>	: dump LUT" << endl
    << "./LUT <board address> e <filename> <out_suff>       : emulateLUT" << endl << endl
        << "board address	: '0x'+(4char)" << endl
        << "filename		: input file for config, output file for dump" << endl
	<< "sectorID		: 0 or 1" << endl
	<< "LUT_type		: 1(BW_LUT), 2(NSW_pos), 3(NSW_angle), 4(pt_merger)" << endl
	<< "RoI			: 0,1,2,...,74,75 for BW_LUT  0,4,8,...,72 for others" << endl
    << "out_suff    : suffix of output file. ex) ./tmp/emu_ " << endl;
}

int main(int argc, char *argv[])
{
  if(argc < 3) {
    show_usage();
    return 0;
  }  
    
  char * invalid_string;
  uint32_t baseaddr = strtol( argv[1],0,16);
  uint32_t BASE_ADDRESS = (baseaddr << 16);
  uint32_t window_size = 0x100000;
  NSLModule interface(BASE_ADDRESS);
  
  char *input_filename = NULL;
  input_filename = argv[3];
  if(string(argv[2]) == "c") interface.ConfigureLUT(input_filename);
  else if(string(argv[2]) == "d"){
	if(argc < 7){
		show_usage();
		return 0;
	}
	int secID = atoi(argv[4]);
	int LUT_type = atoi(argv[5]);
	int RoI = atoi(argv[6]);
	if(!interface.dumpLUT(input_filename, secID, LUT_type, RoI)){
		show_usage();
		return 0;
	}
  }
  else if(string(argv[2]) == "e"){
    if(argc < 5){
        show_usage();
        return 0;
    }
    std::string out_suff = string(argv[4]);
    interface.emulateLUT(input_filename, out_suff);
  } else {
    cerr << "unknown option, " << argv[2] << endl;
    show_usage();
  }
  return 0;
}
