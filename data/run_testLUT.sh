#!/bin/bash

rm input_test.txt

####copy LUT configuration files. Specify the target files.

#cp -p ~/software/DB_LUT/yoshida/DBFiles/BW_address/test_LUT.txt ./test_LUT_BW.txt
#cp -p ~/software/DB_LUT/hibi/TGCNSW_CW_DB_pT20_10_address/testLUT_NSW_pos.txt .
#cp -p ~/software/DB_LUT/hibi/TGCNSW_CW_DB_pT20_10_address/testLUT_NSW_ang.txt .
#cp -p ~/software/DB_LUT/pt_merger/pt_merger_test.txt .

####Merge LUT configuration files into ONE file.
cat test_LUT_BW.txt >> input_test.txt
cat test_LUT_BW.txt >> input_test.txt
cat testLUT_NSW_pos.txt >> input_test.txt
cat testLUT_NSW_pos.txt >> input_test.txt
cat testLUT_NSW_ang.txt >> input_test.txt
cat testLUT_NSW_ang.txt >> input_test.txt
cat pt_merger_test.txt  >> input_test.txt
cat pt_merger_test.txt  >> input_test.txt
