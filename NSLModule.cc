// NSLModule.cc
#include <stdint.h>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <iomanip>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <chrono>
#include <string>
#include <sstream>

#include "NSLModule.hh"

#define DEVICE "/dev/vmedrv32d16"
#define SIZE 0x100000

using namespace std;

NSLModule::NSLModule( off_t vmeaddr )
{

	fd = open( DEVICE, O_RDWR ); //O_RDWR means "open for R/W" 
	if ( fd == -1 ) {
		std::cerr << "ERROR: cannot open " << DEVICE << std::endl;
		exit(1);
	}
	
	base_address =
	static_cast<uint16_t*>( mmap( 0, SIZE, PROT_READ | PROT_WRITE,
                                      MAP_SHARED, fd, vmeaddr) );
                                       
	if ( base_address == (uint16_t*)-1 ) {
		std::cerr << "ERROR: cannot mmap" << std::endl;
		exit(1);
	}
	
}

NSLModule::~NSLModule()
{
	munmap( base_address, SIZE );
	close( fd );
}

NSLModule::result NSLModule::writevme( chip_id chip, unsigned long address,
                                       uint16_t value )
{
	if ( (chip == CPLD) & (address > 65535) )
	return INVALID_ADDRESS;
	
	if ( (chip == FPGA) & (address > 65535) )
	return INVALID_ADDRESS;
	
	switch ( chip ) {
		case CPLD:
		  base_address[address >> 1] = value;
		break;
		
		case FPGA:
		  base_address[(address + 0x4) >> 1] = value;
		break;
		
		default:
		return INVALID_CHIP_ID;
    }
	return NO_ERROR;
}

NSLModule::result NSLModule::readvme( chip_id chip, unsigned long address,
                                      uint16_t * value_p )
{
	if ( (chip == CPLD) & (address > 65535) )
	return INVALID_ADDRESS;
	
	if ( (chip == FPGA) & (address > 65535) )
	return INVALID_ADDRESS;
         
	switch ( chip ) {
		case CPLD:
		  *value_p = base_address[address >> 1];
		break;
		
		case FPGA:
		  *value_p = base_address[(address + 0x4) >> 1];
		break;
		
		default:
		return INVALID_CHIP_ID;
	}
	return NO_ERROR;
}

NSLModule::result NSLModule::erase()
{
	uint16_t check;
	int count = 0;
	
	writevme( CPLD, 0x8000, 0x80 );
	usleep(1);
	writevme( CPLD, 0x8000, 0x0 );
	count = 0;
	while( true ) {
		readvme( CPLD, 0x8000, &check );
		if( !( check & 4UL ) )
		break;
		count++;
		if( count == 20000 ) {
			std::cout << "init high timeout" << std::endl;
			return ERASE_FAIL;
		}
	}
	
	readvme( CPLD, 0x8000, &check );
	if ( check & 1UL ) 
	  return ERASE_FAIL;
	
	else 
	  return NO_ERROR;
        
}

NSLModule::result NSLModule::configure( const char * filename )
{
	std::ifstream bitfile( filename );
	char data1, data0;
	uint16_t cfgdata;
	uint16_t check;
	if ( !bitfile )
	return CANNOT_OPEN_FILE;
	
	char check_data[8] = {0,0,0,0,0,0,0,0};
	int count = 0;
	while ( !( ( check_data[7] == (char)0xff )
			& ( check_data[6] == (char)0xff )
			& ( check_data[5] == (char)0xff )
			& ( check_data[4] == (char)0xff )
			& ( check_data[3] == (char)0xaa )
			& ( check_data[2] == (char)0x99 )
			& ( check_data[1] == (char)0x55 )
			& ( check_data[0] == (char)0x66 ) ) )
	{
		check_data[7] = check_data[6];
		check_data[6] = check_data[5];
		check_data[5] = check_data[4];
		check_data[4] = check_data[3];
		check_data[3] = check_data[2];
		check_data[2] = check_data[1];
		check_data[1] = check_data[0];
		check_data[0] = bitfile.get();
		++count;
		if ( count > 200 )
		return INVALID_FILE;
	}	
	bitfile.seekg( -36, std::ios::cur );

        // reset
	writevme( CPLD, 0x0010, 0x0 );
	writevme( CPLD, 0x0010, 0x1 );
	writevme( CPLD, 0x0010, 0x1 );
	writevme( CPLD, 0x0020, 0x136 );

        // erase
	writevme( CPLD, 0x8000, 0x0 );

	usleep(1);
	writevme( CPLD, 0x8000, 0x80 );
	usleep(600);

	count = 0;
	while( true ) {
		readvme( CPLD, 0x8000, &check );
		if( ( check & 4UL ) )
		break;
		count++;
		if( count == 20000 ) {
			std::cout << "init high timeout" << std::endl;
			return CONF_FAIL;
		}
	}
 
	// configure
	count = 0;
	while( true ) {
		data1 = bitfile.get();
		data0 = bitfile.get();
		if( bitfile.eof() )
		break;
	
		writevme( CPLD, 0x100, bitswap( data1, data0 ) );
		count++;
		if(count % 100 == 0){
		  std::cout << count <<std::endl;
		}

	}

	readvme( CPLD, 0x8000, &check );
	std::cout << "done check = "<< std::hex << check << std::endl;
	if ( check & 1UL )
	return NO_ERROR;
	
	else
	return CONF_FAIL;
}

uint16_t NSLModule::bitswap( char data1, char data0 )
{
	uint16_t cfgdata;

	cfgdata =
	( ( data1 &   1UL ) << 15 ) |
	( ( data1 &   2UL ) << 13 ) |
	( ( data1 &   4UL ) << 11 ) |
	( ( data1 &   8UL ) <<  9 ) |
	( ( data1 &  16UL ) <<  7 ) |
	( ( data1 &  32UL ) <<  5 ) |
	( ( data1 &  64UL ) <<  3 ) |
	( ( data1 & 128UL ) <<  1 ) |
	( ( data0 &   1UL ) <<  7 ) |
	( ( data0 &   2UL ) <<  5 ) |
	( ( data0 &   4UL ) <<  3 ) |
	( ( data0 &   8UL ) <<  1 ) |
	( ( data0 &  16UL ) >>  1 ) |
	( ( data0 &  32UL ) >>  3 ) |
	( ( data0 &  64UL ) >>  5 ) |
	( ( data0 & 128UL ) >>  7 ) ;
	
	return cfgdata;
}



bool NSLModule::Bpi_loadBitfile()
{
  usleep(100);
  uint32_t AddrFpga = 0x8000;
  uint32_t AddrBus = 0x0020;
  uint16_t value = 0;
  uint16_t address = 0x0;
  int count = 0;

  writevme(CPLD, AddrBus, 0x0132);
  readvme(CPLD, AddrBus, &address);
  std::cout << "Config mode = " << address << std::endl;
  writevme(CPLD, AddrFpga, 0x001f);
  readvme(CPLD, AddrFpga, &address);
  std::cout << "FPGA status : " << address << std::endl;
  writevme(CPLD, AddrFpga, 0x009f);
  for(int i=0; i<100; i++){
    readvme(CPLD, AddrFpga, &value);
    if(value == 0x009f){
      std::cout << "FPGA configuration is Success." << std::endl;
      std::cout << "Configure time is  " << count * 0.1 << " s." << std::endl;
      break;
    }
    else{
      usleep(100000); // 0.1s
      ++count;
    }
  }
  writevme(CPLD, AddrBus, 0x0025);

  return true;
}


bool NSLModule::Bpi_initial()
{
  uint16_t value = 0;
  std::cout << "Bus configuring..." << std::endl;
  writevme(CPLD, 0x0020, (uint16_t)0x0025);
  // Initial setup                                                                                                                                                                                      
  writevme(CPLD, 0x0040, 0x0011);
  writevme(CPLD, 0x8000, 0x001f);
  //                                                                                                                                                                                                    
  readvme(CPLD, 0x0020, &value);
  std::cout << std::hex << value << std::endl;
  std::cout << "Changed the bus configure register" << std::endl;
  if(value != 0x0025) return false;

  return true;
}


bool NSLModule::Bpi_unlock(int size_t)
{
  int size = size_t;
  uint32_t addr2 = 0x0810;
  uint32_t bpi = 0x0008;
  uint16_t value = 0;
  
  writevme(CPLD, addr2, 0);
  std::cout << "Unlock the BPI" << std::endl;
  for(int i=0; i<size+1; ++i){
    writevme(CPLD, addr2, i);
    writevme(CPLD, bpi, 0x0060); // Block lock setup
    writevme(CPLD, bpi, 0x00D0); // Block unlock
    
    readvme(CPLD, bpi, &value);
    if(value != 0x80){ return false; } 
    writevme(CPLD, bpi, 0x0050); // clear status register
  }
  std::cout << "Unlocled the BPI" << std::endl;

  return true;
}

bool NSLModule::Bpi_erase(int size_t)
{
  uint32_t addr1 = 0x0800;
  uint32_t addr2 = 0x0810;
  uint32_t bpi = 0x0008;
  uint16_t address = 0x0;
  uint16_t value = 0;
  int size = size_t;
  std::cout << "Erase the BPI" << std::endl;
  
  int count = 0;
  int count2 = 0;
  while(count < size+1){
    writevme(CPLD, addr1, 0x0000);
    writevme(CPLD, addr2, count);
    writevme(CPLD, bpi, 0x0020);
    readvme(CPLD, addr2, &address);
    readvme(CPLD, bpi, &value);
    std:: cout << "Address : " << address << ", value : " << value << std::endl;
    writevme(CPLD, bpi, 0x00D0);
    sleep(1);
    if(value == 0x0080){
      ++count;
      count2 = 0;
    }
    else if(value == 0x0000){
      readvme(CPLD, bpi, &value);
      ++count2;
    }
    else if(value == 0xFFFF){
      writevme(CPLD, 0x0040, 0x0011);
      ++count2;
    }
    else if(value == 0xa2){
      writevme(CPLD, 0x0040, 0x0);
      usleep(1);
      writevme(CPLD, 0x0040, 0x1);
      writevme(CPLD, addr2, 0x0);
      ++count2;
    }
    else{
      ++count2;
      if(count2 > 10)
	break;
    }
  } 
    
  writevme(CPLD, bpi, 0x0050); // clear status register
  std::cout << "Erased the BPI" << std::endl;

  return true;
}

bool NSLModule::Bpi_Buff_Prog(uint32_t size_t, const std::string bitfilename)
{
  std::cout << "bit path: " << bitfilename <<std::endl;
  std::ifstream bitfile(bitfilename.c_str());
  if(!bitfile){
    std::cerr << "L1MuEVmeApi::configureBpi : bitfile does NOT exist" << std::endl;
    return false;
  }
  
  int size = size_t;
  uint32_t addr1 = 0x0800;
  uint32_t addr2 = 0x0810;
  uint32_t bpi  = 0x0008;
  uint16_t count = 0;
  u_char check_data[8] = {0,0,0,0,0,0,0,0}; // sizeof(char) = 1

  std::cout << "Programming now... wait for a while." << std::endl;

  while (
         !(   ( check_data[7] == (u_char)0xff )
              & ( check_data[6] == (u_char)0xff )
              & ( check_data[5] == (u_char)0xff )
              & ( check_data[4] == (u_char)0xff )
              & ( check_data[3] == (u_char)0xaa )
              & ( check_data[2] == (u_char)0x99 )
              & ( check_data[1] == (u_char)0x55 )
              & ( check_data[0] == (u_char)0x66 )
              )
         ){
    check_data[7] = check_data[6];
    check_data[6] = check_data[5];
    check_data[5] = check_data[4];
    check_data[4] = check_data[3];
    check_data[3] = check_data[2];
    check_data[2] = check_data[1];
    check_data[1] = check_data[0];
    check_data[0] = bitfile.get();
    
    ++count;
    if ( count > 200 ) {
      std::cerr << "L1MuEVmeApi::configureFpgaSelect :initialize bitfile failed" << std::endl;
      return false;
    }
  }
  
  bitfile.seekg(-52, std::ios::cur); // back to the bus width detect pattern   default number = -36
  u_char data1, data0;
  uint16_t cfgdata;

  for(int k = 0; k < size+1; k++){ // Number of blocks
    writevme(CPLD, addr2, k);
      for(int i=0; i < 128 ; i++){  // 1 block
	writevme(CPLD, bpi, 0x00E8);
	writevme(CPLD, bpi, 511); // 512 words writing
	for(int j=0; j<512; ++j){
	  int n = 512*i + j;
	  data1 = bitfile.get();
	  data0 = bitfile.get();
	  
	  if( bitfile.eof() ) break;
	  cfgdata =
	    ( ( data1 &   1UL ) << 15 ) |
	    ( ( data1 &   2UL ) << 13 ) |
	    ( ( data1 &   4UL ) << 11 ) |
	    ( ( data1 &   8UL ) <<  9 ) |
	    ( ( data1 &  16UL ) <<  7 ) |
	    ( ( data1 &  32UL ) <<  5 ) |
	    ( ( data1 &  64UL ) <<  3 ) |
	    ( ( data1 & 128UL ) <<  1 ) |
	    ( ( data0 &   1UL ) <<  7 ) |
	    ( ( data0 &   2UL ) <<  5 ) |
	    ( ( data0 &   4UL ) <<  3 ) |
	    ( ( data0 &   8UL ) <<  1 ) |
	    ( ( data0 &  16UL ) >>  1 ) |
	    ( ( data0 &  32UL ) >>  3 ) |
	    ( ( data0 &  64UL ) >>  5 ) |
	    ( ( data0 & 128UL ) >>  7 );

	  
	  if(j % 511 == 0 && j != 0){    
	    writevme(CPLD, addr1, n);
	    writevme(CPLD, bpi, cfgdata);
	    writevme(CPLD, bpi, 0x00D0);
	    usleep(900);
	    writevme(CPLD, bpi, 0x0050);
	  }
	  
	  else{
	    writevme(CPLD, addr1, n);
	    writevme(CPLD, bpi, cfgdata);
	  }
	  int count1 = 1;
	  if(bitfile.peek() == std::ios::traits_type::eof() ){
	    for(int l=0; l < 511-j; l++){
	      int m = 512*i+j+count1;
	      writevme(CPLD, addr1, m);
	      writevme(CPLD, bpi, 0xFFFF);
	      count1++;
	    }
	    writevme(CPLD, bpi, 0x00D0);
	    usleep(900);
	    writevme(CPLD, bpi, 0x0050);
	    break;
	  } // end of eof
	  
	} // end j
      } // end i      
  }   // end k
  

  return true;    
} // end prog_buff



bool NSLModule::ConfigureLUT(char *input_filename = NULL)
{
  uint32_t AddrBus = 0x0020;
  writevme(CPLD, AddrBus, 0x0125);

  // LUT init reset, reset all FIFOs etc.
  writevme(FPGA, 0x10f0, 0x1);
  writevme(FPGA, 0x10f0, 0x0);

  // LUT init mode to "Write"
  writevme(FPGA, 0xa000, 0x1);
  usleep(100);

  std::ifstream reading_file(input_filename);
  std::string data_str;
  std::string field;
  
  int data_num = 0;
  int data_int = 0;
  
  int total_number = 0;
  int write_counter = 0;

  int LUT_num[4] = {76, 19, 19, 19}; // {BW, NSW pos, NSW angle, pt merger}
  int LUT_addr[4] = {4096, 8192, 16384, 4096};

  for(int LUT_type = 0; LUT_type < 4; LUT_type++){
  	int total_LUT_type = 0;
	for(int secID = 0; secID < 2; secID++){
		int write_num = 0;
		for(int num = 0; num < LUT_num[LUT_type]; num++){
			for(int addr = 0; addr < LUT_addr[LUT_type];){
				if (data_num==0){
					std::getline(reading_file,data_str);
					if (!reading_file.eof()){
						std::istringstream stream2(data_str);
						std::getline(stream2, field, ':');
						data_num = std::strtol(field.c_str(), NULL, 10);
						std::getline(stream2, field, ':');
						data_int = std::strtol(field.c_str(),NULL,16);
					}else{
						data_int = 2;
						std::cerr << "File reached to EOF, but data writing still continue" << std::endl;
					}
				}
				writevme(FPGA, 0xa010, data_int);
				total_number++;
				write_num++;
				data_num--;
                write_counter++;
                if(write_counter == 900){
                    //usleep(1000);
                    write_counter = 0;
                }
				if(LUT_type==0) addr+=3;
				else		addr+=4;
			}
		}
        uint16_t cnt0, cnt1;
        readvme(FPGA, 0x0010, &cnt0);
        readvme(FPGA, 0x0020, &cnt1);
        int cnt = (cnt1 << 16) | cnt0;

        std::cout << cnt0 << "   " << cnt1 << std::endl;
		std::cout << "type:  " << LUT_type << "  secID:  " << secID << "  write_num:  " << write_num << "    real write:  " << cnt << std::endl;
		total_LUT_type += write_num;

	}
	std::cout << "type:  " << LUT_type << "   " << total_LUT_type << std::endl;
  }
  reading_file.close();

  // LUT init mode to "Operation"
  sleep(5);
  writevme(FPGA, 0xa000, 0x0); 

  std::cout << "total number of write: " << total_number << std::endl;
  return true;
}

bool NSLModule::dumpLUT(char *filename = NULL, int secID = 0, int LUT_type = 0, int RoI = 0)
{

  if( !(secID==0 || secID==1) ||
      !(1 <= LUT_type && LUT_type <= 4) ||
      (LUT_type == 1 && !(0 <= RoI && RoI < 76) ) ||
      (LUT_type != 1 && (RoI%4 != 0 || !(0 <= RoI && RoI <= 72) ))
    ){
      std::cerr << "Invalid parameters." << std::endl;
      return false;
  }

  uint32_t AddrBus = 0x0020;
  writevme(CPLD, AddrBus, 0x0125);
  writevme(FPGA, 0x0000, 0xabcd);

  // LUT init reset, reset all FIFOs etc.
  writevme(FPGA, 0x10f0, 0x1);
  writevme(FPGA, 0x10f0, 0x0);

  // LUT init mode to "Read"
  writevme(FPGA, 0xa000, 0x2);

  uint16_t rd_param;
  uint16_t outdata;
  uint16_t outaddress;

  rd_param = ((RoI & 0xFF) << 8) | ((LUT_type & 0x7) << 4) | (secID & 0x1);
  std::cout << std::hex<<rd_param << std::endl;
  //rd_param = ((RoI & 0xFF) << 8) | ((LUT_type & 0x7) << 4) | 0x1 ;// | (secID & 0x1);
  //rd_param = 0xFFFF;
  writevme(FPGA, 0xa080, rd_param);
  usleep(5);
  uint16_t rd_param_tmp;
  readvme(FPGA, 0xa080, &rd_param_tmp);
  int _roi, luttype, secid;
  secid = rd_param_tmp & 0x1;
  luttype = (rd_param_tmp >> 4) & 0x7;
  _roi = (rd_param_tmp >> 8) & 0xFF;
  std::cout << "roi = " << _roi << "  luttype = " << luttype << "  secid = " << secid << std::endl;
  std::ofstream writing_file;
  writing_file.open(filename, std::ios::out);
  if(writing_file.fail()){
    std::cerr << "Output file cannot open." << std::endl;
    return false;
  }

  writing_file << "#RoI LUT_type secID" << std::endl
	       << _roi << ' ' << luttype << ' ' << secid << std::endl;

  int depth[4] = {4096, 8192, 16384, 4096};

  for (int address = 0; address < depth[LUT_type-1]; address++){
    writevme(FPGA, 0xa070, address);
    if (filename != NULL){
      readvme(FPGA, 0xa060, &outdata);
      readvme(FPGA, 0xa070, &outaddress);
      //std::cout 
	  //  << std::hex << std::setw(4) << std::setfill('0') << outaddress << ' '
	  //  << std::hex << std::setw(4) << std::setfill('0') << outdata << ' '
	  //  << std::endl;

      writing_file 
	    << std::hex << std::setw(4) << std::setfill('0') << outaddress << ' '
	    << std::hex << std::setw(4) << std::setfill('0') << outdata << ' '
	    << std::endl;
    }
  }

  writevme(FPGA, 0xa000, 0x0); // LUT init mode to "Operation"
  writing_file.close();
  return true;
}

bool NSLModule::emulateLUT(char *input_filename=NULL, const std::string &out_suff="./"){

    if(input_filename==NULL){
        std::cerr << "No input filename" << std::endl;
        return false;
    }

    const int MAX_DATA = 480016;
    std::ifstream reading_file(input_filename);
    if(reading_file.fail()){
        std::cerr << "Input file cannot open" << std::endl;
        return false;
    }

    int LUT_num[4] = {76, 19, 19, 19}; // {BW, NSW pos, NSW angle, pt merger}
    int LUT_addr[4] = {4096, 8192, 16384, 4096};
    std::string type[4] = {"BW", "NSW_POS", "NSW_ANGLE", "PT_MERGER"};


    int data_num = 0;
    int data_int = 0;
    std::string data_str, field;
    for(int LUT_type = 0; LUT_type < 4; LUT_type++){
    	int total_LUT_type = 0;
      for(int secID = 1; secID >= 0; secID--){
      	int write_num = 0;
        int CNT = 0;
      	for(int num = 0; num < LUT_num[LUT_type]; num++){
            int roi = num;
            if(type[LUT_type] == "BW") roi = num;
            else                       roi = num * 4;
            std::ostringstream oss;
            oss << std::setw(3) << std::setfill('0') << roi;
            std::string out_name = out_suff + type[LUT_type] + "_RoI" + oss.str() + "_" + "S" + to_string(secID) + ".txt";

            ofstream ofs(out_name.c_str());
            std::cout << "emulating " << out_name << std::endl;
            ofs << "#RoI LUT_type secID" << std::endl
                << roi << ' ' << LUT_type+1 << ' ' << secID << std::endl;

      		for(int addr = 0; addr < LUT_addr[LUT_type];){
      			if (data_num==0){
      				std::getline(reading_file,data_str);
      				if (!reading_file.eof()){
      					std::istringstream stream2(data_str);
      					std::getline(stream2, field, ':');
      					data_num = std::strtol(field.c_str(), NULL, 10);
      					std::getline(stream2, field, ':');
      					data_int = std::strtol(field.c_str(),NULL,16);
                        CNT++;
      				}else{
      					data_int = 2;
      				}
      			}
                data_num--;
                int data0, data1, data2, data3;
      			if(LUT_type==0){
                    data0 = data_int & 0x1f;
                    data1 = (data_int >> 5) & 0x1f;
                    data2 = (data_int >> 10) & 0x1f;
                    if(addr<LUT_addr[LUT_type])
                        ofs 
                            << std::hex << std::setw(4) << std::setfill('0') << addr << ' '
                            << std::hex << std::setw(4) << std::setfill('0') << data0 << ' '
                            << std::endl;
                    if(addr+1<LUT_addr[LUT_type])
                        ofs 
                            << std::hex << std::setw(4) << std::setfill('0') << addr+1 << ' '
                            << std::hex << std::setw(4) << std::setfill('0') << data1 << ' '
                            << std::endl;
                    if(addr+2<LUT_addr[LUT_type])
                        ofs 
                            << std::hex << std::setw(4) << std::setfill('0') << addr+2 << ' '
                            << std::hex << std::setw(4) << std::setfill('0') << data2 << ' '
                            << std::endl;

                    addr+=3;
                }else{		
                    data0 = data_int & 0xf;
                    data1 = (data_int >> 4) & 0xf;
                    data2 = (data_int >> 8) & 0xf;
                    data3 = (data_int >> 12) & 0xf;
                    ofs 
                        << std::hex << std::setw(4) << std::setfill('0') << addr << ' '
                        << std::hex << std::setw(4) << std::setfill('0') << data0 << ' '
                        << std::endl;
                    ofs 
                        << std::hex << std::setw(4) << std::setfill('0') << addr+1 << ' '
                        << std::hex << std::setw(4) << std::setfill('0') << data1 << ' '
                        << std::endl;
                    ofs 
                        << std::hex << std::setw(4) << std::setfill('0') << addr+2 << ' '
                        << std::hex << std::setw(4) << std::setfill('0') << data2 << ' '
                        << std::endl;
                    ofs 
                        << std::hex << std::setw(4) << std::setfill('0') << addr+3 << ' '
                        << std::hex << std::setw(4) << std::setfill('0') << data3 << ' '
                        << std::endl;

                    addr+=4;
                }
      		}
      	}
        total_LUT_type += CNT;
	    std::cout << "type:  " << LUT_type << "   " << total_LUT_type << std::endl;
      }
    }

    return true;
}
