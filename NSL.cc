// NSL.cc
#include <stdint.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <cstdlib>

#include "NSLModule.hh"

using namespace std;

enum { CONFIGURE, ERASE, READ, WRITE } operation;
unsigned long BASE_ADDRESS = 0x01000000;  // default Address
unsigned long address = 0;
uint16_t value = 0;
char * filename = 0;

NSLModule::chip_id chip;
NSLModule::result return_code;

void show_usage( void )
{
	cerr << endl
	<< "./NSL <operation> (filename) <board address> [arguments]" << endl
	<< endl
	<< "./NSL c <filename> <board address>                                 :configure FPGA" << endl
	<< "./NSL e            <board address>                                 :erase FPGA" << endl
	<< "./NSL r            <board address> <chip> <device address>         :read"  << endl
	<< "./NSL w            <board address> <chip> <device address> <value> :write" << endl
	<< endl
	<< "board address  : '0x'+(4char)" << endl
	<< "chip           : f(FPGA) or c(CPLD)" << endl
	<< "device address : '0x'+(4char) " << endl
	<< "value          : '0x'+(4char)" << endl
	<< endl;
}

void get_operation( char * argument )
{
	if ( string( argument ) == "c" ) {
		operation = CONFIGURE;
		return;
	} else if ( string( argument ) == "e" ) {
		operation = ERASE;
		return;
	} else if ( string( argument ) == "r" ) {
		operation = READ;
		return;
	} else if ( string( argument ) == "w" ) {
		operation = WRITE;
		return;
	} else {
		show_usage();
		exit( 1 );
	cout << "Class" << endl;
	}
}

void get_chip_id( char * argument )
{
	if ( string( argument ) == "f" ) {
		chip = NSLModule::FPGA;
		return;
	} else if ( string( argument ) == "c" ) {
		chip = NSLModule::CPLD;
		return;
	} else {
		show_usage();
		exit( 1 );
	}
}

void get_address( char * argument )
{
	char * invalid_string;
	unsigned long pre_address;
	pre_address = strtoul( argument, &invalid_string, 0 );
	
	if ( ( *invalid_string == '\0' )
		&& ( pre_address <= 65535 ) )
	{
		address = pre_address;
		return;
	} else {
		show_usage();
		exit( 1 );
	}
}

void get_NSLAddress( char * argument )
{
	char * invalid_string;
	unsigned long pre_address;
	
	pre_address = strtoul( argument, &invalid_string, 0 );
	
	if ( ( *invalid_string == '\0' ) && ( pre_address <= 65535 ) ) {
		BASE_ADDRESS = ( pre_address << 16 );
		return;
	} else {
		show_usage();
		exit( 1 );
	}
}

void get_value( char * argument )
{
	unsigned long pre_value;
	char * invalid_string;
	pre_value = strtoul( argument, &invalid_string, 0 );
	if ( *invalid_string == '\0' ) {
		value = pre_value;
		return;
	} else {
		show_usage();
		exit( 1 );
	}
}

void set_operation( int argc, char * argv[] )
{
	if ( argc <= 2 ) {
		show_usage();
		exit( 1 );
	}
	get_operation( argv[1] );
	switch ( operation ) {
		case CONFIGURE:
		if ( argc != 4 ) {
				show_usage();
				exit( 1 );
		}
		filename = argv[2];
		get_NSLAddress( argv[3] );
		return;
		
		case ERASE:
		if ( argc !=3 ) {
			show_usage();
			exit( 1 );
		}
		get_NSLAddress( argv[2] );
		return;
		
		case READ:
		if ( argc !=5 ) {
			show_usage();
			exit( 1 );
		}
		get_NSLAddress( argv[2] );
		get_chip_id( argv[3] );
		get_address( argv[4] );
		return;
		
		case WRITE:
		if ( argc !=6 ) {
			show_usage();
			exit( 1 );
		}
		get_NSLAddress( argv[2] );
		get_chip_id( argv[3] );
		get_address( argv[4] );
		get_value( argv[5] );
		return;
		
		default: // do nothing ( impossible option )
		exit( 10 );
	}
}

int main( int argc, char * argv[] )
{
  //	cout << "Start" << endl;
	set_operation( argc, argv );
	NSLModule NSL( BASE_ADDRESS );
	switch ( operation ) {
		case CONFIGURE:
		return_code = NSL.configure( filename );
		switch ( return_code ) {
			case NSLModule::NO_ERROR:
			cerr << "configure: complete" << endl;
			return 0;
			
			case NSLModule::CANNOT_OPEN_FILE:
			cerr << "configure: cannot open " << filename <<endl;
			return -1;
			
			case NSLModule::INVALID_FILE:
			cerr << "configure: invalid file" << endl;
			return -1;
			
			case NSLModule::CONF_FAIL:
			cerr << "configure: failure" << endl;
			return -1;
			
			default: // do nothing ( impossible option )
			return -10;
		}
		return -10;
		
		case ERASE:
		return_code = NSL.erase();
		switch ( return_code ) {
			case NSLModule::NO_ERROR:
			cerr << "erase: complete" << endl;
			return 0;
			
			case NSLModule::ERASE_FAIL:
			cerr << "erase: failure" << endl;
			return -1;
			
			default: // do nothing ( impossible option )
			return -10;
		}
		return -10;
		
		case READ:
		return_code = NSL.readvme( chip, address, &value );
		switch ( return_code ) {
			case NSLModule::NO_ERROR:
			cerr << "read: chip = " << argv[3] << ", "
			<< "address = " << setw( 2 ) << address << ", "
			<< "value = 0x" << setw( 4 ) << setfill('0') << hex << value << endl;
			return 0;
			
			case NSLModule::INVALID_CHIP_ID:
			cerr << "read: invalid chip ID" << endl
			<< "f(FPGA), c(CPLD) are allowed" << endl;
			return -1;
			
			case NSLModule::INVALID_ADDRESS:
			cerr << "read: invalid address" << endl
			<< "[0-65535] for CPLD," << endl
			<< "[0-65535] for FPGA" << endl;
			return -1;
			
			default: // do nothing ( impossible option )
			return -10;
		}
		return -10;
		
		case WRITE:
		return_code = NSL.writevme( chip, address, value );
		switch ( return_code ) {
			case NSLModule::NO_ERROR:
			cerr << "write: chip = " << argv[3] << ", "
			<< "address = " << setw( 2 ) << address << ", "
			<< "value = 0x"
			<< setw( 4 ) << setfill('0') << hex
			<< value << endl;
			return 0;
			
			case NSLModule::INVALID_CHIP_ID:
			cerr << "write: invalid chip ID" << endl
			<< "f(FPGA), c(CPLD) are allowed" << endl;
			return -1;
			
			case NSLModule::INVALID_ADDRESS:
			cerr << "write: invalid address" << endl
			<< "[0-65535] for CPLD," << endl
			<< "[0-65535] for FPGA" << endl;
			return -1;
			
			default: // do nothing ( impossible option )
			return -10;
		}
		return -10;
		
		default: // do nothing ( impossible option )
		return -10;
	}
}
