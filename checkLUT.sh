#!/bin/bash

emu_dir=./emu
dump_dir=./dmp
emu_suff=${emu_dir}/emu_TDP_0
dump_suff=${dump_dir}/dmp_TDP_0
baddr=0x0144
inputfile="./data/input_test.txt"

if [ ! -e ${emu_dir} ] ; then
    echo "Making directory ${emu_dir}"
    mkdir -p ${emu_dir}
fi

if [ ! -e ${dump_dir} ] ; then
    echo "Making directory ${dump_dir}"
    mkdir -p ${dump_dir}
fi

rm -f ${emu_dir}/${emu_suff}*
rm -f ${dump_dir}/${dump_suff}*

time ./LUT ${baddr} e ${inputfile} ${emu_suff} >& /dev/null
time ./LUT ${baddr} c ${inputfile} >&2

# LOOP for BW
echo "BW dumping"
for secID in `seq 0 1`; do
    for roi in `seq -f %03g 0 75`; do
        emu_file=${emu_suff}BW_RoI${roi}_S${secID}.txt
        dump_file=${dump_suff}BW_RoI${roi}_S${secID}.txt

        ./LUT ${baddr} d ${dump_file} ${secID} 1 ${roi} >& /dev/null
        echo -n "${emu_file}  ${dump_file}  "
        diff -bB ${emu_file} ${dump_file} | sed '/^[^>].*/d' | wc -l
    done
done

# LOOP for NSW POS
echo "NSW_POS dumping"
for secID in `seq 0 1`; do
    for roi in `seq -f %03g 0 4 72`; do
        emu_file=${emu_suff}NSW_POS_RoI${roi}_S${secID}.txt
        dump_file=${dump_suff}NSW_POS_RoI${roi}_S${secID}.txt

        ./LUT ${baddr} d ${dump_file} ${secID} 2 ${roi} >& /dev/null
        echo -n "${emu_file}  ${dump_file}  "
        diff -bB ${emu_file} ${dump_file} | sed '/^[^>].*/d' | wc -l
    done
done

# LOOP for NSW ANGLE
echo "NSW_ANG dumping"
for secID in `seq 0 1`; do
    for roi in `seq -f %03g 0 4 72`; do
        emu_file=${emu_suff}NSW_ANGLE_RoI${roi}_S${secID}.txt
        dump_file=${dump_suff}NSW_ANGLE_RoI${roi}_S${secID}.txt

        ./LUT ${baddr} d ${dump_file} ${secID} 3 ${roi} >& /dev/null
        echo -n "${emu_file}  ${dump_file}  "
        diff -bB ${emu_file} ${dump_file} | sed '/^[^>].*/d' | wc -l
    done
done

# LOOP for pt merger
echo "PT_MERGER dumping"
for secID in `seq 0 1`; do
    for roi in `seq -f %03g 0 4 72`; do
        emu_file=${emu_suff}PT_MERGER_RoI${roi}_S${secID}.txt
        dump_file=${dump_suff}PT_MERGER_RoI${roi}_S${secID}.txt

        ./LUT ${baddr} d ${dump_file} ${secID} 4 ${roi} >& /dev/null
        echo -n "${emu_file}  ${dump_file}  "
        diff -bB ${emu_file} ${dump_file} | sed '/^[^>].*/d' | wc -l
    done
done
