# Change Log
## [Bit3Software-01-00-00]
### NSLModule
- Initial Commit
- Contains functions for NSL Configure, read/write registers, BPI Configure, and LUT config/dump

### NSL
- Initial Commit

### BPI
- Initial Connit

### LUT
- Initial Commit
- Runs for L1MuESLFirmware in this [commit](https://gitlab.cern.ch/atlas-l1muon/endcap/firmware/L1MuESLFirmware/tree/a1df87762450c1ff503b0029f6743bb02ee6bb0d), L1MuESLFirmware-sakatsuk-LutConfig-02-02-04.bit.
