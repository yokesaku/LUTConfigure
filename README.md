# Bit3Software
## Overview  
This repository manages test codes that can be used via bit3.

## Description
### NSLModule.cc, NSLModule.hh
Contains the main functions.

### NSL.cc
Configure FPGA, or read/write signle register on FPGA/CPLD. Usage:
```
./NSL <operation> (filename) <board address> [arguments]
./NSL c <filename> <board address>                                 :configure FPGA
./NSL e            <board address>                                 :erase FPGA
./NSL r            <board address> <chip> <device address>         :read
./NSL w            <board address> <chip> <device address> <value> :write
board address  : '0x'+(4char)
chip           : f(FPGA) or c(CPLD)
device address : '0x'+(4char) 
value          : '0x'+(4char)
```

### BPI.cc
Configure BPI via VME, then configure FPGA fromo the design stored in BPI. Usage:
```
./BPI <filename> <board address> 
filename      : path to the bitfile
board address : '0x'+(4char)
```


### LUT.cc
Configure FPGA LUT from data stored in a text file.
Under development. Usage:
```
./LUT <board address> <operation> <filename> 
board address  : '0x'+(4char)
operation      : c for config, d for dump
filename       : input file for config, output file for dump
```

## Compile
Simply type `make` to compile. Need to `make clean` after updates.
Need C++11 or newer, so if g++ is older than 4.7, then you'll need to install 4.8.
See, e.g. [here](https://www.task-notes.com/entry/20151114/1447492231) or [here](https://qiita.com/phanect/items/45c67fa55d624e45367b).