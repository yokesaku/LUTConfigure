// NSLModule.hh

#ifndef NSLMODULE_HH
#define NSLMODULE_HH

#include <sys/types.h>

class NSLModule
{
public:
  enum result{ NO_ERROR,
	       INVALID_CHIP_ID,
	       INVALID_ADDRESS,
	       CANNOT_OPEN_FILE,
	       INVALID_FILE,
	       CONF_FAIL,
	       ERASE_FAIL };
  enum chip_id{ FPGA, CPLD };
  NSLModule( off_t address );
  virtual ~NSLModule( void );
  result writevme( chip_id chip, unsigned long address,
		   uint16_t value );
  result readvme( chip_id chip, unsigned long address,
		  uint16_t * value_p );
  result configure( const char * filename );
  result erase();
  uint16_t bitswap( char data1, char data0 );
  bool Bpi_loadBitfile();
  bool Bpi_initial();
  bool Bpi_unlock(int size_t);
  bool Bpi_erase(int size_t);
  bool Bpi_Buff_Prog(uint32_t size_t, const std::string bitfilename);
  bool ConfigureLUT(char *input_filename);
  bool dumpLUT(char *input_filename, int secID, int LUT_type, int RoI);
  bool emulateLUT(char *input_filename, const std::string &out_suff);
  
private:
  int fd;
  uint16_t * base_address;
};

#endif
// NSLMODULE_HH
