// BPI.cc
#include <stdint.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <cstdlib>
#include <fstream>

#include "NSLModule.hh"

using namespace std;

void BPi_usage()
{
  std::cout 
    << endl
    << "./BPI <filename> <board address> " << endl
    << endl
    << "filename      : path to the bitfile" << endl
    << "board address : '0x'+(4char)" << endl
    << endl;
}
int main(int argc, char* argv[])
{

  if(argc != 3){
    BPi_usage();
    return -1;
  }

  uint32_t baseaddr = strtol(argv[2],0,16);
  uint32_t BASE_ADDRESS = (baseaddr << 16);

  uint32_t window_size = 0x100000;
  NSLModule interface(BASE_ADDRESS);

  const char * filename = argv[1];

  std::ifstream bitfile(filename);

  std::streamsize size = bitfile.seekg(0, std::ios::end).tellg();

  std::cout << "Bitfile size = " << std:: dec << size/1000 << "kByte" << std::endl;
  int Size = size*8/1048576; // Byte to Mbit
  std::cout << "Configuratio file size is " << Size << "Mbit" << std::endl;
  bitfile.seekg(0, std::ios::beg);

  // ****************************************************************************** //

  interface.Bpi_initial();
  
  interface.Bpi_unlock(Size);

  std::cout << std::hex << Size << " blocks erase." << std::endl;
  interface.Bpi_erase(Size);

  interface.writevme(NSLModule::CPLD, 0x0800, 0x0000); // set the BPI address: 0x000000

  interface.Bpi_Buff_Prog(Size, filename);
  


  uint16_t value;
  interface.readvme(NSLModule::CPLD, 0x0008, &value);
  std::cout << std::hex << value << std::endl;
  
    
  interface.writevme(NSLModule::CPLD, 0x0008, 0x0050);
  interface.writevme(NSLModule::CPLD, 0x0008, 0x0050);
  interface.readvme(NSLModule::CPLD, 0x0008, &value);
  std::cout << std::hex << value << std::endl;
  

  interface.writevme(NSLModule::CPLD, 0x0800, 0);
  interface.writevme(NSLModule::CPLD, 0x0810, 0);


  interface.Bpi_loadBitfile();
  interface.writevme(NSLModule::CPLD, 0x0020, 0x0025);

  interface.readvme(NSLModule::CPLD, 0x8000, &value);
  if(value == 0x009f){
    std::cout << "Finish the program successfully" << std::endl;
  }
  else{
    std::cout << "Dumping BPI containts..." << std::endl;
  }

  return 0;   
}
