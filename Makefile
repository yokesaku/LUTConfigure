CC=g++ 
CFLAG=-g
TARGETS = NSL BPI LUT
OBJS = $(addsuffix .o, $(TARGETS))
SRCS = $(addsuffix .cc, $(TARGETS))
DEPS=NSLModule.o

all: $(TARGETS)

$(TARGETS): $(OBJS) $(DEPS)
	$(CC) -std=c++11 -o $@ $@.o $(DEPS)

#$(OBJS): $(SRCS)
#	$(CC) -std=c++11 -o $@ -c $<

%.o: %.cc
	$(CC) -std=c++11 -o $@ -c $<



#$(OBJGROUP): NSLModule.cc
#	$(CC) -std=c++11 -o $@ -c $^

clean:
	rm -f $(TARGETS) $(OBJS) $(DEPS)
